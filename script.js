var app = angular.module('hangman', []);

app.factory('myService', function ($http) {
    return {
        getWord: function () {
            return $http.get(
                'http://api.wordnik.com:80/v4/words.json/randomWord?hasDictionaryDef=false&minCorpusCount=0&maxCorpusCount=-1&minDictionaryCount=1&maxDictionaryCount=-1&minLength=5&maxLength=-1&api_key=a2a73e7b926c924fad7001ca3111acd55af2ffabf50eb4ae5'
            )
        }
    };
});


/**
 *
 */
app.controller('HangmanController', function (myService, $scope, $sce, $rootScope) {

    var data = '';
    var index = 0;
    var fail = 0;
    var arrayWord = [];

    $scope.model = {
        data: '',
        length: 0,
        random: [],
        onKeyUpResult: '',
        wordKeyCodes: [],
        status: 0,
        html: [],
        key: '',
        wrong: false,
        clear: '',
        end: false,
        flow: [
            'left-foot','right-foot','left-leg','right-leg','left-hand','right-hand',
            'left-arm','right-arm','corpus-folk','neck-folk','head-folk'
        ].reverse()
    };


    myService.getWord().then(function (d) {
        data = d.data.word;
        $scope.randomLetter();
    });

    //window.onkeydown = keydown();

    /*$rootScope.$on('keypress', function (evt, obj, key) {
        $scope.$apply(function () {
            $scope.key = key;
        });
        alert('ok');
    });*/

    $scope.randomLetter = function () {
        var wordLength = data.length;
        var randLetter = [];
        var randomIndexLetter = 0;
        arrayWord = $scope.wordToArray(data);
        var randomWord = '';
        var randomWordArray = [];

        while (wordLength != randLetter.length) {
            randomIndexLetter = Math.floor((Math.random() * wordLength) + 1);       //shuffle index between count word letter

            if ($scope.inArray(randomIndexLetter, randLetter) == false) {           //check if it not exist letter on this index
                randLetter.push(randomIndexLetter);
            }

        }

        // add shuffle letters
        for (var i = 0; i < randLetter.length; i++) {
            randomWord += arrayWord[randLetter[i] - 1];
            $scope.model.random.push(arrayWord[randLetter[i] - 1]);
        }

        //set data to view
        $scope.model.data = randomWord;
        $scope.model.length = randLetter;
    };

    //search value in array
    $scope.inArray = function (item, array) {
        return (-1 !== array.indexOf(item));
    };

    //convert string to char array
    $scope.wordToArray = function (array) {
        var splitWord = [];

        for (var i = 0; i < array.length; i++) {
            splitWord.push(array[i]);
        }

        return splitWord;
    };

    $scope.getKeyboardEventResult = function (keyEvent, keyEventDesc) {
        return (window.event ? keyEvent.keyCode : keyEvent.which);
        //return keyEventDesc + " (keyCode: " + (window.event ? keyEvent.keyCode : keyEvent.which) + ")";
    };

    $scope.onKeyUp = function ($event) {
        $scope.onKeyUpResult = $scope.getKeyboardEventResult($event, "Key up");
        //alert($scope.onKeyUpResult);
        var charKeyUp = String.fromCharCode($scope.onKeyUpResult).toLocaleLowerCase();
        var smallLetters = $scope.arrayToLower(arrayWord);

        if (charKeyUp == smallLetters[index]) {
            $scope.model.html += '<li class="properly">'+smallLetters[index]+'</li>';
            index++;
            $scope.model.wrong = false;
            $scope.model.clear = '';
        }
        else {
            $scope.model.flow.pop();                //pop part of folk if we faild
            fail++;
            $scope.model.wrong = true;
        }

        if ($scope.model.flow.length == 0) {        //no part of folk, game over
            $scope.model.end = true;
            //alert('game-over');
        }
    };

    $scope.arrayToLower = function (words) {
        var small = [];

        for (var i = 0; i < words.length; i++) {
            small.push(words[i].toLowerCase());
        }

        return small;
    };

    $scope.getHtml = function(html){
        return $sce.trustAsHtml(html);
    };

    $scope.start = function() {

        $scope.model.end = false;
        $scope.model.wrong = false;
        $scope.model.clear = '';
        $scope.model.flow = [
            'left-foot','right-foot','left-leg','right-leg','left-hand','right-hand',
            'left-arm','right-arm','corpus-folk','neck-folk','head-folk'
        ].reverse()

        myService.getWord().then(function (d) {
            data = d.data.word;
            $scope.randomLetter();
        });
    };

});

